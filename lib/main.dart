import 'package:flutter/material.dart';
import 'package:flutter_application_2/pages/easy_level.dart';
import 'package:flutter_application_2/pages/hard_level.dart';
import 'package:flutter_application_2/pages/normal_level.dart';
import 'package:flutter_application_2/pages/rusult.dart';
import 'package:flutter_application_2/resource/seample_binding.dart';
import 'package:flutter_application_2/start_page.dart';

//import 'package:flutter_application_2/start_page.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
//import 'package:hive/hive.dart';

//import 'package:randomizer/randomizer.dart';

void main() async {
  await Hive.initFlutter();
  runApp(GetMaterialApp(
    //theme: ThemeData(primaryIconTheme: ),
    debugShowCheckedModeBanner: false,
    home: StartPage(),
    initialRoute: '/hello',
    getPages: [
      GetPage(name: '/hello', page: () => StartPage(), binding: SampleBind()),
      GetPage(name: '/easy', page: () => EasyLevel(), binding: SampleBind()),
      GetPage(
          name: '/middle', page: () => NormalLevel(), binding: SampleBind()),
      GetPage(name: '/hard', page: () => HardLevel(), binding: SampleBind()),
      GetPage(name: '/result', page: () => Result(), binding: SampleBind()),
    ],
  ));
}
