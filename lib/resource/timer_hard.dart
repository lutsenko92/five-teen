import 'dart:async';
import 'package:get/get.dart';

class MyTimerHard extends GetxController {
  var timer = Timer.periodic(Duration(seconds: 1), (timer) {}).obs;
  var countedSeconds = 0.obs;
  var timedDuration = Duration.zero.obs;
  var timerRunning = false;

  void stepTimer() {
    //timedDuration.value = Duration(seconds: countedSeconds.value);
    countedSeconds.value++;
    timedDuration.value = Duration(seconds: countedSeconds.value);
  }

  void startTimer() {
    timerRunning = true;
    timer.value.cancel();
    countedSeconds.value = 0;
    //timedDuration.value = Duration(seconds: countedSeconds.value);
    timer.value = Timer.periodic(Duration(seconds: 1), (timer) {
      stepTimer();
    });
  }

  void stopTimer() {
    timerRunning = false;
    timer.value.cancel();
  }
}
