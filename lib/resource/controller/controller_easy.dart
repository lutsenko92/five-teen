import 'package:flutter_application_2/resource/timer_easy.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class ControllerEasy extends GetxController {
  var timer = Get.put(MyTimerEasy()).obs;
  var numbers = List.filled(9, "").obs;
  var selectedIndex1 = (-1).obs;
  var buffer = "".obs;
  var t = 8.obs;
  var enother = true.obs;
  var buffer2 = "0".obs;
  var finishGame = false.obs;

  @override
  void onInit() {
    ifMayWin();
    super.onInit();
  }

  ifMayWin() {
    int inv = 0;
    do {
      inv = 0;
      firstInit();
      for (int i = 0; i < 9; i++) {
        if (numbers[i] != "") {
          for (int j = 0; j < i; j++) {
            if (int.parse(numbers[j]) > int.parse(numbers[i])) {
              inv++;
            }
          }
        }
      }
    } while (inv % 2 == 1);
  }

  firstInit() {
    timer.value.countedSeconds.value = 0;
    timer.value.timedDuration.value =
        Duration(seconds: timer.value.countedSeconds.value);
    timer.value.startTimer();
    t.value = 8;
    for (int i = 0; i < 9; i++) {
      numbers[i] = "";
    }
    numbers[0] = (Random().nextInt(7) + 1).toString();
    for (int i = 1; i < 8; i++) {
      while (numbers[i] == "") {
        buffer2.value = ((Random().nextInt(100) % 8) + 1).toString();
        if (numbers.contains(buffer2.value) == false) {
          numbers[i] = buffer2.value;
        }
      }
    }
  }

  runBricks(BuildContext context, int index) {
    if (selectedIndex1.value == -1) {
      selectedIndex1.value = index;
    }

    if (selectedIndex1.value == (t.value - 1) ||
        selectedIndex1.value == t.value + 1 ||
        selectedIndex1.value == t.value - 3 ||
        selectedIndex1.value == t.value + 3) {
      if (selectedIndex1.value != 2 || t.value != 3) {
        if (selectedIndex1.value != 3 || t.value != 2) {
          if (selectedIndex1.value != 5 || t.value != 6) {
            if (selectedIndex1.value != 6 || t.value != 5) {
              buffer.value = (numbers[t.value]);
              numbers[t.value] = numbers[selectedIndex1.value];
              numbers[selectedIndex1.value] = buffer.value;
              t.value = selectedIndex1.value;
            }
          }
        }
      }
    }
    selectedIndex1.value = -1;

    for (int i = 0; i < 8; i++) {
      if (numbers[i] == "") {
        finishGame.value = false;
        break;
      } else {
        if (i == int.parse(numbers[i]) - 1 || i == int.parse(numbers[i])) {
          finishGame.value = true;
        } else {
          finishGame.value = false;
          break;
        }
      }
    }
    if (finishGame.value == true) {
      selectedIndex1.value = -5;
      timer.value.stopTimer();
      Get.toNamed("/result");
    }
  }
}
