import 'package:flutter_application_2/resource/timer_hard.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class ControllerHard extends GetxController {
  var timer = Get.put(MyTimerHard()).obs;
  var numbers = List.filled(25, "").obs;
  var selectedIndex1 = (-1).obs;
  var buffer = "".obs;
  var t = 24.obs;
  var enother = true.obs;
  var buffer2 = "0".obs;
  var finishGame = false.obs;

  @override
  void onInit() {
    ifMayWin();
    timer.value.startTimer();
    super.onInit();
  }

  ifMayWin() {
    int inv = 0;
    do {
      inv = 0;
      firstInit();
      for (int i = 0; i < 25; i++) {
        if (numbers[i] != "") {
          for (int j = 0; j < i; j++) {
            if (int.parse(numbers[j]) > int.parse(numbers[i])) {
              inv++;
            }
          }
        }
      }
    } while (inv % 2 == 1);
  }

  firstInit() {
    timer.value.countedSeconds.value = 0;
    timer.value.timedDuration.value =
        Duration(seconds: timer.value.countedSeconds.value);
    timer.value.startTimer();
    t.value = 24;
    for (int i = 0; i < 25; i++) {
      numbers[i] = "";
    }
    numbers[0] = (Random().nextInt(23) + 1).toString();
    for (int i = 1; i < 24; i++) {
      while (numbers[i] == "") {
        buffer2.value = ((Random().nextInt(100) % 24) + 1).toString();
        if (numbers.contains(buffer2.value) == false) {
          numbers[i] = buffer2.value;
        }
      }
    }
  }

  runBricks(BuildContext context, int index) {
    if (selectedIndex1.value == -1) {
      selectedIndex1.value = index;
    }

    if (selectedIndex1.value == (t.value - 1) ||
        selectedIndex1.value == t.value + 1 ||
        selectedIndex1.value == t.value - 5 ||
        selectedIndex1.value == t.value + 5) {
      if (selectedIndex1.value != 4 || t.value != 5) {
        if (selectedIndex1.value != 5 || t.value != 4) {
          if (selectedIndex1.value != 9 || t.value != 10) {
            if (selectedIndex1.value != 10 || t.value != 9) {
              if (selectedIndex1.value != 14 || t.value != 15) {
                if (selectedIndex1.value != 15 || t.value != 14) {
                  if (selectedIndex1.value != 19 || t.value != 20) {
                    if (selectedIndex1.value != 20 || t.value != 19) {
                      buffer.value = (numbers[t.value]);
                      numbers[t.value] = numbers[selectedIndex1.value];
                      numbers[selectedIndex1.value] = buffer.value;
                      t.value = selectedIndex1.value;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    selectedIndex1.value = -1;

    for (int i = 0; i < 24; i++) {
      if (numbers[i] == "") {
        finishGame.value = false;
        break;
      } else {
        if (i == int.parse(numbers[i]) - 1 || i == int.parse(numbers[i])) {
          finishGame.value = true;
        } else {
          finishGame.value = false;
          break;
        }
      }
    }
    if (finishGame.value == true) {
      selectedIndex1.value = -5;
      timer.value.stopTimer();
      Get.toNamed("/result");
    }
  }
}
