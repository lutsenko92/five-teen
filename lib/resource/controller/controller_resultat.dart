import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_application_2/pages/easy_level.dart';
import 'package:flutter_application_2/pages/hard_level.dart';
import 'package:flutter_application_2/pages/normal_level.dart';
import 'package:flutter_application_2/resource/confeti.dart';
//import 'package:flutter_application_2/resource/timer.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/scheduler.dart';
import 'package:hive/hive.dart';
//import 'dart:math';
import 'package:confetti/confetti.dart';

class ResultController extends GetxController {
  final Confeti confeti = Confeti();
  final TextEditingController controller = TextEditingController();
  final easyController = Get.find<EasyLevel>();
  final normalController = Get.find<NormalLevel>();
  final hardController = Get.find<HardLevel>();
  var userWinner = "".obs;
  var winnerEasyList = List.filled(5, "").obs;
  var winnerNormalList = List.filled(5, "").obs;
  var winnerHardList = List.filled(5, "").obs;
  var timeWinnerEasyList = List.filled(5, 0).obs;
  var timeWinnerNormalList = List.filled(5, 0).obs;
  var timeWinnerHardList = List.filled(5, 0).obs;
  var timeEasy = 0.obs;
  var timeNormal = 0.obs;
  var timeHard = 0.obs;
  var go = 0.obs;
  var valueDialog = "".obs;

  @override
  void onInit() async {
    super.onInit();
    var box = await Hive.openBox('BoxResult');
    if (await box.get('nameEasy') == null) {
      box.put('nameEasy', List.filled(5, ""));
    }
    if (await box.get('nameNormal') == null) {
      box.put('nameNormal', List.filled(5, ""));
    }
    if (await box.get('nameHard') == null) {
      box.put('nameHard', List.filled(5, ""));
    }
    if (await box.get('timeEasy') == null) {
      box.put('timeEasy', List.filled(5, 0));
    }
    if (await box.get('timeNormal') == null) {
      box.put('timeNormal', List.filled(5, 0));
    }
    if (await box.get('timeHard') == null) {
      box.put('timeHard', List.filled(5, 0));
    }

    winnerEasyList.value = await box.get('nameEasy');
    winnerNormalList.value = await box.get('nameNormal');
    winnerHardList.value = await box.get('nameHard');
    timeWinnerEasyList.value = await box.get('timeEasy');
    timeWinnerNormalList.value = await box.get('timeNormal');
    timeWinnerHardList.value = await box.get('timeHard');

    if (easyController.contE.finishGame.value) {
      writeWinnerEasy();
    }
    if (normalController.contN.finishGame.value) {
      writeWinnerNormal();
    }
    if (hardController.contH.finishGame.value) {
      writeWinnerHard();
    }
    // shoD(0); // DELETE THIS. For TEST
  }

  writeWinnerEasy() {
    timeEasy.value = easyController.contE.timer.value.countedSeconds.value;
    if (timeEasy.value > 1) {
      if (timeWinnerEasyList[0] == 0) {
        timeWinnerEasyList[0] = timeEasy.value;
        WidgetsBinding.instance.addPostFrameCallback((_) => shoD(0));
        //winnerEasyList[0] = valueDialog.value;
        go.value = 1;
      }
      if (go.value != 1) {
        for (int i = 0; i < 5; i++) {
          if (go.value != 1) {
            if (timeEasy.value <= timeWinnerEasyList[i] ||
                timeWinnerEasyList[i] == 0) {
              for (int j = 3; j >= i; j--) {
                timeWinnerEasyList[j + 1] = timeWinnerEasyList[j];
                winnerEasyList[j + 1] = winnerEasyList[j];
              }
              winnerEasyList[i] = "";
              WidgetsBinding.instance.addPostFrameCallback((_) => shoD(i));
              //winnerEasyList[i] = valueDialog.value;
              timeWinnerEasyList[i] = timeEasy.value;
              go.value = 1;
            }
          }
        }
      }
      //recordWin();
      go.value = 0;
    }
  }

  writeWinnerNormal() {
    timeNormal.value = normalController.contN.timer.value.countedSeconds.value;
    if (timeNormal.value > 1) {
      if (timeWinnerNormalList[0] == 0) {
        timeWinnerNormalList[0] = timeNormal.value;
        WidgetsBinding.instance.addPostFrameCallback((_) => shoD(0));
        go.value = 1;
      }
      if (go.value != 1) {
        for (int i = 0; i < 5; i++) {
          if (go.value != 1) {
            if (timeNormal.value <= timeWinnerNormalList[i] ||
                timeWinnerEasyList[i] == 0) {
              for (int j = 3; j >= i; j--) {
                timeWinnerNormalList[j + 1] = timeWinnerNormalList[j];
                winnerNormalList[j + 1] = winnerNormalList[j];
              }
              winnerNormalList[i] = "";
              WidgetsBinding.instance.addPostFrameCallback((_) => shoD(i));
              //winnerNormalList[i] = valueDialog.value;
              timeWinnerNormalList[i] = timeNormal.value;
              go.value = 1;
            }
          }
        }
      }
      //recordWin();
      go.value = 0;
    }
  }

  writeWinnerHard() {
    timeHard.value = hardController.contH.timer.value.countedSeconds.value;
    if (timeHard.value > 1) {
      if (timeWinnerHardList[0] == 0) {
        timeWinnerHardList[0] = timeHard.value;
        WidgetsBinding.instance.addPostFrameCallback((_) => shoD(0));
        go.value = 1;
      }
      if (go.value != 1) {
        for (int i = 0; i < 5; i++) {
          if (go.value != 1) {
            if (timeHard.value <= timeWinnerHardList[i] ||
                timeWinnerEasyList[i] == 0) {
              for (int j = 3; j >= i; j--) {
                timeWinnerHardList[j + 1] = timeWinnerHardList[j];
                winnerHardList[j + 1] = winnerHardList[j];
              }
              winnerHardList[i] = "";
              WidgetsBinding.instance.addPostFrameCallback((_) => shoD(i));
              //winnerHardList[i] = valueDialog.value;
              timeWinnerHardList[i] = timeHard.value;
              go.value = 1;
            }
          }
        }
      }
      //recordWin();
      go.value = 0;
    }
  }

  void recordWin() {
    var box = Hive.box('BoxResult');
    box.put('nameEasy', winnerEasyList.value);
    box.put('nameNormal', winnerNormalList.value);
    box.put('nameHard', winnerHardList.value);
    box.put('timeEasy', timeWinnerEasyList.value);
    box.put('timeNormal', timeWinnerNormalList.value);
    box.put('timeHard', timeWinnerHardList.value);
  }

  void shoD(int i) {
    confeti.controllerCenter.play();
    Get.defaultDialog(
        title: "CONGRATULATIONS",
        backgroundColor: Colors.orange[400],
        titleStyle: TextStyle(color: Colors.white),
        //middleText: "Enter your name",
        //middleTextStyle: TextStyle(color: Colors.white),
        //textConfirm: "OK",
        //onConfirm: () {
        //recordWin();
        // Get.back();
        //},
        /*textCancel: "Cancel",
        cancelTextColor: Colors.white,*/
        //confirmTextColor: Colors.white,
        //buttonColor: Colors.blue[800],
        barrierDismissible: false,
        radius: 15,
        content: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Container(
              width: Get.width * 0.8,
              height: Get.width * 0.5,
              child: Image.asset(
                'assets/images/wood_background.jpg',
                fit: BoxFit.fill,
              ),
            ),
            Container(
              color: Color.fromRGBO(255, 255, 255, 0.3),
            ),
            ConfettiWidget(
              confettiController: confeti.controllerCenter,
              blastDirectionality: BlastDirectionality
                  .explosive, // don't specify a direction, blast randomly
              shouldLoop:
                  true, // start again as soon as the animation is finished
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ], // manually specify the colors to be used
              createParticlePath:
                  confeti.drawStar, // define a custom shape/path.
            ),
            Column(
              children: [
                Text(''),
                Text("YOU WIN!!!",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 25)),
                Text("Enter your name:",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 20)),
                TextField(
                  //initialValue: 'ENTER YOUR NAME',
                  autofocus: true,
                  textAlign: TextAlign.center,
                  cursorColor: Colors.white,
                  style: TextStyle(
                      fontSize: 22,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                  maxLength: 14,
                  controller: controller,
                  onChanged: (String value) {
                    if (easyController.contE.finishGame.value) {
                      winnerEasyList[i] = value;
                    }
                    if (normalController.contN.finishGame.value) {
                      winnerNormalList[i] = value;
                    }
                    if (hardController.contH.finishGame.value) {
                      winnerHardList[i] = value;
                    }
                  },
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.orangeAccent[400]),
                    onPressed: () {
                      recordWin();
                      Get.back();
                    },
                    child: Text('OK',
                        style: TextStyle(color: Colors.white, fontSize: 22)))
              ],
            ),
          ],
        ));
  }
}
