import 'package:flutter_application_2/pages/easy_level.dart';
import 'package:flutter_application_2/pages/hard_level.dart';
import 'package:flutter_application_2/pages/normal_level.dart';
import 'package:flutter_application_2/pages/rusult.dart';

import 'package:flutter_application_2/start_page.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StartPage>(() => StartPage());
    Get.lazyPut<EasyLevel>(() => EasyLevel());
    Get.lazyPut<NormalLevel>(() => NormalLevel());
    Get.lazyPut<HardLevel>(() => HardLevel());
    Get.lazyPut<Result>(() => Result());
  }
}
