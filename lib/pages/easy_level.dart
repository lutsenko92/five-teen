import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_2/resource/controller/controller_easy.dart';
import 'package:get/get.dart';
//import 'package:flutter/src/painting/box_fit.dart';

class EasyLevel extends StatelessWidget {
  final ControllerEasy contE = Get.put(ControllerEasy());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 0, 0, 0.0001),
        title: Text(
          "F!VEteen",
          style: TextStyle(fontSize: 25),
        ),
        centerTitle: true,
      ),*/
      body: Stack(
        children: [
          Container(
            width: Get.width,
            height: Get.height,
            child: Image.asset(
              'assets/images/wood_background.jpg',
              fit: BoxFit.fill,
            ),
          ),
          Column(
            children: [
              Text(
                '',
                style: TextStyle(fontSize: 22),
              ),
              Expanded(
                flex: 1,
                child: RichText(
                  text: TextSpan(
                      text: "F!VE",
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                        //fontFamily: 'DancingScript'
                      ),
                      children: [
                        TextSpan(
                          text: "teen",
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                              fontFamily: 'DancingScript'),
                        )
                      ]),
                ),
              ),
              Expanded(
                flex: 9,
                child: SizedBox(
                  //height: 0.2,
                  child: Container(
                    // height: 200,
                    //color: Colors.yellowAccent,
                    padding: const EdgeInsets.all(10.0),
                    child: GridView.builder(
                        // padding: const EdgeInsets.all(10.0),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                mainAxisSpacing: 12.0,
                                crossAxisSpacing: 12.0),
                        itemCount: contE.numbers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              contE.runBricks(context, index);
                            },
                            child: Obx(
                              () => Container(
                                  decoration: BoxDecoration(
                                      border: /*(index == contE.t.value)
                                          ?*/
                                          Border.all(
                                              color: Colors.brown[400],
                                              width: 4) /*: null*/,
                                      image: (index != contE.t.value)
                                          ? DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/one_brick.jpg'))
                                          : null,
                                      /*DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/brick_empty.jpg'))*/

                                      /*gradient: (index != contE.t.value)
                                          ? LinearGradient(
                                              stops: [0.1, 0.5, 0.8],
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                Colors.brown[500],
                                                Colors.brown[700],
                                                Colors.brown[800],
                                              ])
                                          : null,*/
                                      color: (index == contE.t.value)
                                          ? Color.fromRGBO(255, 255, 255, 0.3)
                                          : null,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12))),
                                  child: Center(
                                    child: Obx(() => Text(
                                          contE.numbers[index],
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: 'DancingScript', //???
                                              fontSize: 60,
                                              color: Colors.white),
                                        )),
                                  )),
                            ),
                          );
                        }),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Text(
                        contE.timer.value.timedDuration.value.inMinutes
                            .toString(),
                        style: TextStyle(color: Colors.white, fontSize: 75),
                      ),
                    ),
                    Text(
                      ":",
                      style: TextStyle(color: Colors.white, fontSize: 60),
                    ),
                    Obx(() => Text(
                          contE.timer.value.timedDuration.value.inSeconds
                              .remainder(60)
                              .toString()
                              .padLeft(2, '0'),
                          style: TextStyle(color: Colors.white, fontSize: 75),
                        )),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue[800],
                          ),
                          onPressed: () {
                            contE.ifMayWin();
                          },
                          child: Text(
                            'Refresh',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.red,
                          ),
                          onPressed: () {
                            contE.timer.value.stopTimer();
                            Get.offNamed('/hello');
                          },
                          child: Text(
                            'STOP',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )),
                    ),
                  ],
                ),
              ),
              /*Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [],
              ),*/
            ],
          ),
        ],
      ),
    );
  }
}
