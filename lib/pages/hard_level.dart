import 'package:flutter/material.dart';
import 'package:flutter_application_2/resource/controller/controller_hard.dart';
import 'package:get/get.dart';

class HardLevel extends StatelessWidget {
  final ControllerHard contH = Get.put(ControllerHard());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: Text(
          "F!VEteen",
          style: TextStyle(fontSize: 25),
        ),
        centerTitle: true,
      ),*/
      body: Stack(
        children: [
          Container(
            width: Get.width,
            height: Get.height,
            child: Image.asset(
              'assets/images/wood_background.jpg',
              fit: BoxFit.fill,
            ),
          ),
          Column(
            children: [
              Text(
                '',
                style: TextStyle(fontSize: 22),
              ),
              Expanded(
                flex: 1,
                child: RichText(
                  text: TextSpan(
                      text: "F!VE",
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                        //fontFamily: 'DancingScript'
                      ),
                      children: [
                        TextSpan(
                          text: "teen",
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                              fontFamily: 'DancingScript'),
                        )
                      ]),
                ),
              ),
              Expanded(
                flex: 9,
                child: SizedBox(
                  //height: 200,
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 5,
                                mainAxisSpacing: 12.0,
                                crossAxisSpacing: 12.0),
                        itemCount: contH.numbers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              contH.runBricks(context, index);
                            },
                            child: Obx(
                              () => Container(
                                  decoration: BoxDecoration(
                                      border: /*(index == contE.t.value)
                                          ?*/
                                          Border.all(
                                              color: Colors.brown[400],
                                              width: 4) /*: null*/,
                                      image: (index != contH.t.value)
                                          ? DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/one_brick.jpg'))
                                          : null,
                                      color: (index == contH.t.value)
                                          ? Color.fromRGBO(255, 255, 255, 0.3)
                                          : null,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12))),
                                  child: Center(
                                    child: Obx(() => Text(
                                          contH.numbers[index],
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontFamily: 'DancingScript', //???
                                              fontSize: 38,
                                              color: Colors.white),
                                        )),
                                  )),
                            ),
                          );
                        }),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisSize: MainAxisSize.max,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Text(
                        contH.timer.value.timedDuration.value.inMinutes
                            .toString(),
                        style: TextStyle(color: Colors.white, fontSize: 75),
                      ),
                    ),
                    Text(
                      ":",
                      style: TextStyle(color: Colors.white, fontSize: 60),
                    ),
                    Obx(() => Text(
                          contH.timer.value.timedDuration.value.inSeconds
                              .remainder(60)
                              .toString()
                              .padLeft(2, '0'),
                          style: TextStyle(color: Colors.white, fontSize: 75),
                        )),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue[800],
                        ),
                        onPressed: () {
                          contH.ifMayWin();
                        },
                        child: Text(
                          'Refresh',
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red,
                        ),
                        onPressed: () {
                          contH.timer.value.stopTimer();
                          Get.offNamed('/hello');
                        },
                        child: Text('STOP',
                            style:
                                TextStyle(color: Colors.white, fontSize: 25))),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
