import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_2/resource/controller/controller_resultat.dart';
import 'package:get/get.dart';

class Result extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ResultController>(
      init: ResultController(),
      builder: (result) {
        return Scaffold(
          /*appBar: AppBar(
            backgroundColor: Colors.blue[800],
            centerTitle: true,
            title: Text('F!VEteen'),
            leading: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.blue[800]),
              child: Icon(Icons.close_rounded),
              onPressed: () {
                Get.offAllNamed('/hello');
              },
            ),
          ),*/
          body: Stack(
            children: [
              Container(
                width: Get.width,
                height: Get.height,
                child: Image.asset(
                  'assets/images/wood_background.jpg',
                  fit: BoxFit.fill,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    '',
                    style: TextStyle(fontSize: 22),
                  ),
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                primary: Colors.transparent),
                            child: Icon(Icons.close_rounded),
                            onPressed: () {
                              Get.offAllNamed('/hello');
                            },
                          ),
                        ),
                        Expanded(
                          flex: 10,
                          child: Center(
                            child: RichText(
                              text: TextSpan(
                                  text: "F!VE",
                                  style: TextStyle(
                                    fontSize: 40,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white,
                                    //fontFamily: 'DancingScript'
                                  ),
                                  children: [
                                    TextSpan(
                                      text: "teen      ",
                                      style: TextStyle(
                                          fontSize: 40,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                          fontFamily: 'DancingScript'),
                                    )
                                  ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // ElevatedButton(onPressed: () => {}, child: Text("1111")),
                  Expanded(
                    flex: 3,
                    child: Column(
                      children: [
                        Text(
                          "  Easy Level  ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              height: 1.5,
                              backgroundColor: Colors.white30,
                              fontSize: 22,
                              fontWeight: FontWeight.w800,
                              color: Colors.white),
                        ),
                        Expanded(
                            child: SizedBox(
                          width: 500,
                          child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Text(
                                                (index + 1).toString() + '. ',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 23,
                                                    height: 2.3),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Obx(
                                            () => Column(children: [
                                              Text(
                                                result.winnerEasyList[index],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 23,
                                                    height: 2.3),
                                              ),
                                            ]),
                                          ),
                                          flex: 8,
                                        ),
                                        Expanded(
                                          child: Column(children: [
                                            Obx(
                                              () => Text(
                                                (result.timeWinnerEasyList[
                                                                index] ~/
                                                            60)
                                                        .toString() +
                                                    ':' +
                                                    result.timeWinnerEasyList[
                                                            index]
                                                        .remainder(60)
                                                        .toString()
                                                        .padLeft(2, '0')
                                                        .toString(),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 23,
                                                    height: 2.3),
                                              ),
                                            )
                                          ]),
                                          flex: 2,
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }),
                        )),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: Column(children: [
                        Text(
                          " Normal Level ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              height: 1.5,
                              backgroundColor: Colors.white30,
                              fontSize: 22,
                              fontWeight: FontWeight.w800,
                              color: Colors.white),
                        ),
                        Expanded(
                            child: SizedBox(
                          width: 500,
                          child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Text(
                                                (index + 1).toString() + '. ',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Obx(
                                            () => Column(children: [
                                              Text(
                                                result.winnerNormalList[index],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            ]),
                                          ),
                                          flex: 8,
                                        ),
                                        Expanded(
                                          child: Column(children: [
                                            Obx(
                                              () => Text(
                                                (result.timeWinnerNormalList[
                                                                index] ~/
                                                            60)
                                                        .toString() +
                                                    ':' +
                                                    result.timeWinnerNormalList[
                                                            index]
                                                        .remainder(60)
                                                        .toString()
                                                        .padLeft(2, '0')
                                                        .toString(),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            )
                                          ]),
                                          flex: 2,
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }),
                        )),
                      ])),
                  Expanded(
                      flex: 3,
                      child: Column(children: [
                        Text(
                          " Hard Level ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              height: 1.5,
                              backgroundColor: Colors.white30,
                              fontSize: 22,
                              fontWeight: FontWeight.w800,
                              color: Colors.white),
                        ),
                        Expanded(
                            child: SizedBox(
                          //width: 500,
                          child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (BuildContext context, int index) {
                                return Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              Text(
                                                (index + 1).toString() + '. ',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            ],
                                          ),
                                          flex: 1,
                                        ),
                                        Expanded(
                                          child: Obx(
                                            () => Column(children: [
                                              Text(
                                                result.winnerHardList[index],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            ]),
                                          ),
                                          flex: 8,
                                        ),
                                        Expanded(
                                          child: Column(children: [
                                            Obx(
                                              () => Text(
                                                (result.timeWinnerHardList[
                                                                index] ~/
                                                            60)
                                                        .toString() +
                                                    ':' +
                                                    result.timeWinnerHardList[
                                                            index]
                                                        .remainder(60)
                                                        .toString()
                                                        .padLeft(2, '0')
                                                        .toString(),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                    height: 2.3),
                                              ),
                                            )
                                          ]),
                                          flex: 2,
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              }),
                        )),
                      ]))
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
