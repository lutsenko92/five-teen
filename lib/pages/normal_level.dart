import 'package:flutter/material.dart';
import 'package:flutter_application_2/resource/controller/controller_normal.dart';
import 'package:get/get.dart';

class NormalLevel extends StatelessWidget {
  final ControllerNormal contN = Get.put(ControllerNormal());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: Text(
          "F!VEteen",
          style: TextStyle(fontSize: 25),
        ),
        centerTitle: true,
      ),*/
      body: Stack(
        children: [
          Expanded(
            child: Container(
              width: Get.width,
              height: Get.height,
              child: Image.asset(
                'assets/images/wood_background.jpg',
                fit: BoxFit.fill,
              ),
            ),
          ),
          Column(
            children: [
              Text(
                '',
                style: TextStyle(fontSize: 22),
              ),
              Expanded(
                flex: 1,
                child: RichText(
                  text: TextSpan(
                      text: "F!VE",
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                        //fontFamily: 'DancingScript'
                      ),
                      children: [
                        TextSpan(
                          text: "teen",
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                              fontFamily: 'DancingScript'),
                        )
                      ]),
                ),
              ),
              Expanded(
                flex: 9,
                child: SizedBox(
                  //height: 200,
                  child: Container(
                    //color: Colors.yellowAccent,
                    padding: const EdgeInsets.all(10.0),
                    child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 4,
                                mainAxisSpacing: 12.0,
                                crossAxisSpacing: 12.0),
                        itemCount: contN.numbers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                              onTap: () {
                                contN.runBricks(context, index);
                              },
                              child: Obx(
                                () => Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.brown[400], width: 4),
                                        image: (index != contN.t.value)
                                            ? DecorationImage(
                                                image: AssetImage(
                                                    'assets/images/one_brick.jpg'))
                                            : null,
                                        color: (index == contN.t.value)
                                            ? Color.fromRGBO(255, 255, 255, 0.3)
                                            : null,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(12))),
                                    child: Center(
                                      child: Obx(() => Text(
                                            contN.numbers[index],
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontFamily:
                                                    'DancingScript', //???
                                                fontSize: 57,
                                                color: Colors.white),
                                          )),
                                    )),
                              ));
                        }),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisSize: MainAxisSize.max,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Text(
                        contN.timer.value.timedDuration.value.inMinutes
                            .toString(),
                        style: TextStyle(color: Colors.white, fontSize: 75),
                      ),
                    ),
                    Text(
                      ":",
                      style: TextStyle(color: Colors.white, fontSize: 60),
                    ),
                    Obx(() => Text(
                          contN.timer.value.timedDuration.value.inSeconds
                              .remainder(60)
                              .toString()
                              .padLeft(2, '0'),
                          style: TextStyle(color: Colors.white, fontSize: 75),
                        )),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue[800],
                          ),
                          onPressed: () {
                            contN.ifMayWin();
                          },
                          child: Text(
                            'Refresh',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.red,
                          ),
                          onPressed: () {
                            contN.timer.value.stopTimer();
                            Get.offNamed('/hello');
                          },
                          child: Text('STOP',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 25))),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
