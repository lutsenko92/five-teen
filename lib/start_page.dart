import 'package:flutter/material.dart';
import 'package:flutter_application_2/resource/block_orient.dart';
//import 'package:flutter_application_2/hard_level.dart';
//import 'package:flutter_application_2/medium_level.dart';
//import 'package:flutter_application_2/rusult.dart';
import 'package:get/get.dart';

//import 'easy_level.dart';

class StartPage extends StatelessWidget with PortraitModeMixin {
  const StartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: Colors.white,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Image.asset('assets/images/start_background.jpg'),
          Positioned(
              bottom: 30,
              child: Column(
                children: [
                  Text(
                    "                                ",
                    style: TextStyle(
                        decoration: TextDecoration.lineThrough,
                        inherit: false,
                        decorationThickness: 1.5,
                        fontSize: 32,
                        color: Colors.blue),
                  ),
                  Text(
                    'Select Level:',
                    style: TextStyle(
                        //decoration: TextDecoration.underline,
                        inherit: false,
                        decorationThickness: 1.5,
                        fontSize: 32,
                        color: Colors.blue),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        Get.offAllNamed('/easy');
                      },
                      child: Text(
                        'Easy',
                        style: TextStyle(fontSize: 20),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Get.offAllNamed('/middle');
                      },
                      child: Text(
                        'Normal',
                        style: TextStyle(fontSize: 20),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Get.offAllNamed('/hard');
                      },
                      child: Text(
                        'Hard',
                        style: TextStyle(fontSize: 20),
                      )),
                  Text(
                    '           ',
                    style: TextStyle(
                        decoration: TextDecoration.lineThrough,
                        inherit: false,
                        decorationThickness: 1.5,
                        fontSize: 32,
                        color: Colors.blue),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        Get.toNamed('/result');
                      },
                      child: Text(
                        'Result',
                        style: TextStyle(fontSize: 20),
                      )),
                ],
              )),
        ],
      ),
    );
  }
}
